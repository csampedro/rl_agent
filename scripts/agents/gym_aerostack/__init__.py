from gym.envs.registration import register

register(
     id='aerostack-v0',
     entry_point='gym_aerostack.envs:AerostackEnv',
)

register(
     id='aerostack_image-v0',
     entry_point='gym_aerostack.envs:AerostackImageEnv',
)
