import numpy as np
from gym import utils
import sys
import gym
from gym import spaces
sys.path.append('/opt/ros/kinetic/lib/python2.7/dist-packages')
import rospy
from rl_agent.srv import *
import time
import os

os.environ['OPENAI_LOG_FORMAT'] ='tensorboard'
os.environ['OPENAI_LOGDIR'] = '/home/Developer/results'

class AerostackEnv( gym.Env ):
    def __init__(self):
        rospy.init_node('environment_step_client')
        self.steps = 0
        self.ac_reward = 0

        self.drone_id_namespace = rospy.get_param('/agent/drone_id_namespace')
        self.drone_id_int = rospy.get_param('/agent/droneId')
        self.configs_path = rospy.get_param('/agent/configs_path')  
        self.exp_logger_path = rospy.get_param('/agent/exp_logger_path')
        self.actor_networks_path = rospy.get_param('/agent/actor_networks_path')
        self.agent_type = rospy.get_param('/agent/agent_type')
        self.EPISODES = rospy.get_param('/agent/num_episodes')
        self.NUM_EPISODES_BETWEEN_LOGS = rospy.get_param('/agent/num_episodes_between_logs')
        self.TEST = rospy.get_param('/agent/num_tests')
        self.FREQUENCY = rospy.get_param('/agent/frecuency')
        self.time = time.time()

        self.rl_env_reset_srv_name = '/' +  'rl_env_reset_srv'
        self.rl_env_step_srv_name = '/' + 'rl_env_step_srv'
        self.rl_env_dimensionality_srv_name = '/' + 'rl_env_dimensionality_srv'

        print('rl_env_reset_srv_name: ' + self.rl_env_reset_srv_name)
        print('rl_env_step_srv_name: ' + self.rl_env_step_srv_name)
        print('rl_env_dimensionality_srv_name: ' + self.rl_env_dimensionality_srv_name)

        self._environment_dimensionality_client()

        self.low_state = np.asarray(self.state_min)
        self.high_state = np.asarray(self.state_max)

        self.action_space = spaces.Box(low=self.action_min[0], high=self.action_max[0], shape=(self.action_dim,))
        self.observation_space = spaces.Box(low=self.low_state, high=self.high_state)

        print(self.observation_space)

        self.MAX_LENGTH_EPISODE = self.num_iterations

        self.r = rospy.Rate(self.FREQUENCY)

    def _environment_dimensionality_client(self):
        print('waiting for server (dimensionality_client)...')
        rospy.wait_for_service(self.rl_env_dimensionality_srv_name)
        try:
            environment_dimensionality = rospy.ServiceProxy(self.rl_env_dimensionality_srv_name, EnvDimensionalitySrv)
            resp = environment_dimensionality()
            print(resp)
            self.state_dim = resp.state_dim_lowdim
            self.state_dim_img = resp.state_dim_img
            self.state_min = resp.state_min
            self.state_max = resp.state_max
            self.action_dim = resp.action_dim
            self.action_min = resp.action_min
            self.action_max = resp.action_max
            self.num_iterations = resp.num_iterations

        except rospy.ServiceException as e:
            print("Service call failed: %s" % e)

    def step(self, action):
        self.r.sleep() #Pensar
        rospy.wait_for_service(self.rl_env_step_srv_name)
        self.steps+=1
        try:
            environment_step = rospy.ServiceProxy(self.rl_env_step_srv_name, AgentSrv)
            resp = environment_step(action)
            ob = resp.obs_real
            reward = resp.reward
            done = resp.terminal_state
        except rospy.ServiceException as e:
            print("Service call failed: %s" % e)
        self.ac_reward = self.ac_reward + reward

        if self.steps >= self.MAX_LENGTH_EPISODE : # pensarr*************************************************
            print('Restart')
            done = True
            self.steps = 0


        return ob, reward, done, {}

    def reset(self):
        print(self.ac_reward)
        self.ac_reward = 0

        rospy.wait_for_service(self.rl_env_reset_srv_name)
        try:
            environment_reset = rospy.ServiceProxy(self.rl_env_reset_srv_name, ResetEnvSrv)
            resp = environment_reset()

            print('------- Response (env reset) -------')
            print('resp.state: ', resp.state)
            return resp.state
        except rospy.ServiceException as e:
            print("Service call failed: %s" % e)

    def render(self, mode='human', close=False):
        rospy.wait_for_service('environment_render')
        try:
            environment_render = rospy.ServiceProxy('environment_render', RenderEnvSrv)
            resp = environment_render()

            image_response = resp.img

            cv_bridge_obj = CvBridge()
            image_np = cv_bridge_obj.imgmsg_to_cv2(resp.img, "rgb8")
            print('------- Response (env render) -------')
            print('resp.img (type): ', type(image_np))

            return image_np
        except rospy.ServiceException as e:
            print("Service call failed: %s" % e)

