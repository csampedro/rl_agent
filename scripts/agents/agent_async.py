#!/usr/bin/env python

import rospy
from sensor_msgs.msg import Image
from rl_agent.srv import *

import os,sys,inspect
sys.path.insert(0,os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))))
import multiprocessing as mp

import cv2
from cv_bridge import CvBridge

import time
import numpy as np

from scripts import misc
from scripts import tools
from scripts.tools.experiment_logger import ExperimentLogger
from scripts.misc import filter_env


rl_boost_python_path = rospy.get_param('/agent/rl_boost_python_path')
rl_python_distpackages_path = rospy.get_param('/agent/rl_python_distpackages_path')
sys.path.insert(1, rl_boost_python_path)
sys.path.insert(2, rl_python_distpackages_path)


#from ddpg import *
import gc
gc.enable()

DEBUG_SERVICES_MODE = False
DEBUG_MODE = False
TEST_MODE = False
ENABLE_ITERATION = False
STATE_BASED_ON_IMAGE = False


agent_type = rospy.get_param('/agent/agent_type')

if agent_type == 'ddpg':
	from scripts.agents.ddpg_agent import *
	from scripts.agents.ddpg_agent.ddpg import DDPG
	from scripts.agents.ddpg_agent.trained_actor_network import TrainedActorNetwork
	from scripts.agents.ddpg_agent.trained_actor_network_3hidden import TrainedActorNetwork  #For testing 3 hidden-layered Actor Network
elif agent_type == 'ddpg_chainer':
	from scripts.agents.chainer_agents import *
	from scripts.agents.chainer_agents.ddpg import DDPG_CHAINER
elif agent_type == 'naf':
	from scripts.agents.chainer_agents import *
	from scripts.agents.chainer_agents.naf import NAF
elif agent_type == 'a3c_chainer':
	from scripts.agents.chainer_agents import *
	from scripts.agents.chainer_agents.a3c import A3C_CHAINER
	from chainerrl.experiments.evaluator import AsyncEvaluator
	from chainerrl.misc import async_
	from chainerrl.misc import random_seed




############## Definition of the ROS servicies for cmmunicating with the ENVIRONMENT ##############
def environment_reset_client(rl_env_reset_srv_name):
    rospy.wait_for_service(rl_env_reset_srv_name)
    try:
        environment_reset = rospy.ServiceProxy(rl_env_reset_srv_name, ResetEnvSrv)
        resp = environment_reset()

        print '------- Response (env reset) -------'
        print 'resp.state: ', resp.state

        image_response = resp.img_state
        if(len(image_response) !=0):
            cv_bridge_obj = CvBridge()
            image_ini = cv_bridge_obj.imgmsg_to_cv2(image_response[0], "mono8")
            images_array = np.empty((image_ini.shape[0], image_ini.shape[1], len(image_response)))
            print 'len(image_response): ', len(image_response)
            for i in range(len(image_response)):
                image_np = cv_bridge_obj.imgmsg_to_cv2(image_response[i], "mono8")
                image_np_norm = np.multiply(image_np, 1.0 / 255.0)
                images_array[:,:,i] = np.reshape(image_np_norm, (image_np_norm.shape[0], image_np_norm.shape[1]))
            print 'images_array (shape): ', images_array.shape
            return resp.state, images_array
        else:
            return resp.state, image_response
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e


def environment_step_client(rl_env_step_srv_name, action):
    #print 'waiting for server (step_client)...'
    rospy.wait_for_service(rl_env_step_srv_name)
    try:
        environment_step = rospy.ServiceProxy(rl_env_step_srv_name, AgentSrv)
        resp = environment_step(action)

        if DEBUG_SERVICES_MODE:
            print '------- Response (env step) -------'
            print 'resp.reward: ', resp.reward
            print 'resp.state: ', resp.obs_real
            print 'resp.terminal_state: ', resp.terminal_state
            print 'resp.img (type): ', type(image_np)

        image_response = resp.img
        if(len(image_response) !=0):
            cv_bridge_obj = CvBridge()
            image_ini = cv_bridge_obj.imgmsg_to_cv2(image_response[0], "mono8")
            images_array = np.empty((image_ini.shape[0], image_ini.shape[1], len(image_response)))
            for i in range(len(image_response)):
                image_np = cv_bridge_obj.imgmsg_to_cv2(image_response[i], "mono8")
                image_np_norm = np.multiply(image_np, 1.0 / 255.0)
                images_array[:,:,i] = np.reshape(image_np_norm, (image_np_norm.shape[0], image_np_norm.shape[1]))
            return resp.obs_real, resp.reward, resp.terminal_state, images_array
        else:
            return resp.obs_real, resp.reward, resp.terminal_state, image_response
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e


def environment_dimensionality_client(rl_env_dimensionality_srv_name):
    print 'waiting for server (dimensionality_client)...'
    rospy.wait_for_service(rl_env_dimensionality_srv_name)
    try:
        environment_dimensionality = rospy.ServiceProxy(rl_env_dimensionality_srv_name, EnvDimensionalitySrv)
        resp = environment_dimensionality()

        if DEBUG_SERVICES_MODE:
            print '------- Response (env dimensionality) -------'
            print 'resp.state_dim_lowdim: ', resp.state_dim_lowdim
            print 'resp.state_dim_img: ', resp.state_dim_img
            print 'resp.state_min: ', resp.state_min
            print 'resp.state_max: ', resp.state_max
            print 'resp.action_dim: ', resp.action_dim
            print 'resp.action_min: ', resp.action_min
            print 'resp.action_max: ', resp.action_max
            print 'resp.num_iterations: ', resp.num_iterations

        return resp.state_dim_lowdim, resp.state_dim_img, resp.state_min, resp.state_max, resp.action_dim, resp.action_min, resp.action_max, resp.num_iterations
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e




def extract_shared_objects_from_agent(agent):
    return dict((attr, async_.as_shared_objects(getattr(agent, attr)))
                for attr in agent.shared_attributes)


def set_shared_objects(agent, shared_objects):
    for attr, shared in shared_objects.items():
        new_value = async_.synchronize_to_shared_objects(
            getattr(agent, attr), shared)
        setattr(agent, attr, new_value)





def train_loop(drone_id_int, process_idx, agent, env_info_list, exp_logger, ros_rate, max_episodes=4000, 
                num_episodes_between_logs=200, num_test=10, agent_frecuency=20,
                environment_reset_client = None,
                environment_step_client = None):
     
     
    rl_env_reset_srv_name = '/drone' + str(drone_id_int + process_idx) + '/rl_env_reset_srv'
    rl_env_step_srv_name = '/drone' + str(drone_id_int + process_idx) + '/rl_env_step_srv'
    
    print('rl_env_reset_srv_name: ' + rl_env_reset_srv_name)
    print('rl_env_step_srv_name: ' + rl_env_step_srv_name)
    STATE_ZERO = []
    ACTION_ZERO = []
    for episode in xrange(max_episodes):
        state_resp, img_resp = environment_reset_client(rl_env_reset_srv_name)
        if not STATE_BASED_ON_IMAGE:
            state = np.array(state_resp)
        else:
            state = img_resp

        reward = 0
        if not TEST_MODE:

            print "episode:",episode

            total_reward = 0
            total_q_value_critic = 0.0
            agent_statistics = []

            ############## Train ##############
            for step in xrange(env_info_list[-1]):
                if agent_type == 'ddpg':
                    action = agent.noise_action(state)
                elif agent_type == 'naf' or agent_type == 'ddpg_chainer' or agent_type == 'a3c_chainer':
                    action = agent.act_and_train(state, reward)
                    if not np.isfinite(action).all():
                        action = np.zeros(len(action))
                else:
                    print "AGENT_ERROR: Agent type not recognized"


                next_state_resp, reward, done, img_state = environment_step_client(rl_env_step_srv_name, action)
                if not STATE_BASED_ON_IMAGE:
                    next_state = np.array(next_state_resp)
                else:
                    next_state = img_state


                total_reward += reward

                if agent_type == 'ddpg':
                    agent.perceive(state, action, reward, next_state, done)
                    q_value_critic = agent.average_q_value_critic
                    total_q_value_critic += q_value_critic
                    average_actor_loss = 0
                    average_critic_loss = 0
                elif agent_type == 'ddpg_chainer':
                    agent_statistics = agent.agent.get_statistics()
                    q_value_critic = agent_statistics[0][1]
                    average_actor_loss = agent_statistics[1][1]
                    average_critic_loss = agent_statistics[2][1]
                    total_q_value_critic += q_value_critic
                elif agent_type == 'naf':
                    agent_statistics = agent.agent.get_statistics()
                    q_value_critic = 0
                    total_q_value_critic += q_value_critic
                    average_actor_loss = agent_statistics[1][1]
                    average_critic_loss = 0
                elif agent_type == 'a3c_chainer':
                    agent_statistics = agent.agent.get_statistics()
                    q_value_critic = 0
                    total_q_value_critic += q_value_critic
                    average_actor_loss = 0
                    average_critic_loss = 0
                else:
                    print "AGENT_ERROR: Agent type not recognized"

                state = next_state

                if done:
                    break

                #~ if not ENABLE_ITERATION:
                    #~ print 'ROS sleep()'
                    #~ ros_rate.sleep()

                
                if episode % num_episodes_between_logs == 0:
                    # Log to file
                    data_variables = [episode, reward, total_reward, q_value_critic, total_q_value_critic, average_actor_loss, average_critic_loss]
                    data_variables.extend(state)
                    data_variables.extend(action)
                    exp_logger.log_steps_train(data_variables)
                    

            # Log to file
            #exp_logger.log_train(episode, total_reward, total_q_value_critic)
            if agent_type == 'ddpg_chainer' or agent_type == 'naf' or agent_type == 'a3c_chainer':
                print ('agent_statistics: ', agent_statistics)
            data_variables = [episode, 0, total_reward, 0, total_q_value_critic]
            data_variables.extend(STATE_ZERO)
            data_variables.extend(ACTION_ZERO)
            exp_logger.log_train(data_variables)


        ############## Test ##############
        if episode % num_episodes_between_logs == 0 and episode >= 100 or TEST_MODE:
            total_reward = 0
            for i in xrange(num_test):

                print "Test episode: ", episode + i

                if i == 0 and not TEST_MODE:
                    if agent_type == 'ddpg':
                        agent.actor_network.save_network(episode + i, exp_logger.results_path + exp_logger.mydir)
                    elif agent_type == 'naf' or agent_type == 'ddpg_chainer' or agent_type == 'a3c_chainer':
                        agent.save_agent(episode + i, exp_logger.results_path + exp_logger.mydir)
                    else:
                        print "AGENT_ERROR: Agent type not recognized"

                accum_reward = 0;

                if not TEST_MODE:
                    # Record video
                    print "Recording video: ", episode + i, ".avi"
                    exp_logger.record_video(episode + i)

                state_resp, img_resp = environment_reset_client(rl_env_reset_srv_name)
                if not STATE_BASED_ON_IMAGE:
                    state = np.array(state_resp)
                else:
                    state = img_resp

                for j in xrange(env_info_list[-1]):
                    #env.render()
                    if agent_type == 'ddpg':
                        action = agent.action(state)  # direct action for test
                    elif agent_type == 'naf' or agent_type == 'ddpg_chainer' or agent_type == 'a3c_chainer':
                        action = agent.agent.act(state)  # direct action for test
                    else:
                        print "AGENT_ERROR: Agent type not recognized"

                    state_resp, reward, done, image_state = environment_step_client(environment_step_client, action)
                    if not STATE_BASED_ON_IMAGE:
                        state = np.array(state_resp)
                    else:
                        state = img_state
                    total_reward += reward
                    accum_reward += reward

                    if i==0:
                        data_variables = [episode, reward, accum_reward, 0, 0]
                        data_variables.extend(state)
                        data_variables.extend(action)
                        exp_logger.log_steps_test(data_variables)

                    if done:
                        break

                    # Check time
                    #~ if not ENABLE_ITERATION:
                        #~ ros_rate.sleep()

                if not TEST_MODE:
                    # Stop video
                    exp_logger.stop_video()

                # Log to file
                data_variables = [episode, 0, accum_reward, 0, 0]
                data_variables.extend(STATE_ZERO)
                data_variables.extend(ACTION_ZERO)
                exp_logger.log_test(data_variables)



            average_reward = total_reward/num_test
            print 'episode: ',episode,'Evaluation Average Reward:', average_reward



def main():
    
    drone_id_namespace = rospy.get_param('/agent/drone_id_namespace')
    drone_id_int = rospy.get_param('/agent/droneId')
    num_processes = rospy.get_param('/agent/num_processes')
    configs_path = rospy.get_param('/agent/configs_path')
    exp_logger_path = rospy.get_param('/agent/exp_logger_path')
    actor_networks_path = rospy.get_param('/agent/actor_networks_path')
    MAX_EPISODES = rospy.get_param('/agent/num_episodes')
    NUM_EPISODES_BETWEEN_LOGS = rospy.get_param('/agent/num_episodes_between_logs')
    NUM_TEST = rospy.get_param('/agent/num_tests')
    FREQUENCY = rospy.get_param('/agent/frecuency')
    
    print 'drone_id_namespace: ', drone_id_namespace
    print 'drone_id_int: ', drone_id_int
    print 'num_processes: ', num_processes
    print 'MAX_EPISODES: ', MAX_EPISODES
    print 'NUM_EPISODES_LOGS', NUM_EPISODES_BETWEEN_LOGS
    print 'NUM_TESTS: ', NUM_TEST
    print 'FREQUENCY: ', FREQUENCY
    print 'AGENT TYPE: ', agent_type
    
    
    def Init():
        rl_env_dimensionality_srv_name = '/drone' + str(drone_id_int + 0) + '/rl_env_dimensionality_srv'
        state_dim_lowdim, state_dim_img, state_min, state_max, action_dim, action_min, action_max, num_iterations = environment_dimensionality_client(rl_env_dimensionality_srv_name)

        return [state_dim_lowdim, state_dim_img, state_min, state_max, action_dim, action_min, action_max, num_iterations]
        
    env_info_list = Init()
    print 'Env Info List:'
    print env_info_list

    
    ############## Creation of the AGENT ##############
    if not TEST_MODE:
        if agent_type == 'ddpg':
            agent = DDPG(env_info_list, configs_path)
        elif agent_type == 'naf':
            agent = NAF(env_info_list, configs_path)
        elif agent_type == 'ddpg_chainer':
            agent = DDPG_CHAINER(env_info_list, configs_path)
        elif agent_type == 'a3c_chainer':
            agent = A3C_CHAINER(env_info_list, configs_path)
        else:
            print "AGENT_ERROR: Agent type not recognized"
    else:
        if agent_type == 'ddpg':
            agent = TrainedActorNetwork(env_info_list, actor_networks_path)
        elif agent_type == 'naf':
            agent = NAF(env_info_list, configs_path)
            agent.load_agent(actor_networks_path)
        elif agent_type == 'ddpg_chainer':
            agent = DDPG_CHAINER(env_info_list, configs_path)
            agent.load_agent(actor_networks_path)
        elif agent_type == 'a3c_chainer':
            agent = A3C_CHAINER(env_info_list, configs_path)
            agent.load_agent(actor_networks_path)
        else:
            print "AGENT_ERROR: Agent type not recognized"
    
    
    def run_func(process_idx):

        if not ENABLE_ITERATION:
            rate = rospy.Rate(FREQUENCY)


        # Prevent numpy from using multiple threads
        os.environ['OMP_NUM_THREADS'] = '1'
        
        counter = mp.Value('l', 0)
        episodes_counter = mp.Value('l', 0)
        training_done = mp.Value('b', False)

        shared_objects = extract_shared_objects_from_agent(agent.agent)
        set_shared_objects(agent.agent, shared_objects)
        random_seed.set_random_seed(process_idx)

        local_agent = agent
        local_agent.agent.process_idx = process_idx


        # Include variables episode, reward, total_reward, q_value_critic, total_q_value_critic, state, action
        data_variables = ['episode', 'reward', 'total_reward', 'q_value_critic', 'total_q_value_critic', 'average_actor_loss', 'average_critic_loss']
        STATE_ZERO = []
        ACTION_ZERO = []
        # Fill with state
        for i in range(0, env_info_list[0]):
            data_variables.append('state_' + str(i))
            # Create ZERO vectors
            STATE_ZERO.append(0)
        # Fill with action
        for i in range(0, env_info_list[4]):
            data_variables.append('action_' + str(i))
            # Create ZERO vectors
            ACTION_ZERO.append(0)

        # Create logger
        exp_log = ExperimentLogger(TEST_MODE, exp_logger_path, data_variables)


        # Train Loop                
        train_loop(
                drone_id_int=drone_id_int,
                process_idx = process_idx, 
                agent = local_agent, 
                env_info_list = env_info_list, 
                exp_logger = exp_log, 
                ros_rate = rate, 
                max_episodes = MAX_EPISODES, 
                num_episodes_between_logs = NUM_EPISODES_BETWEEN_LOGS, 
                num_test = NUM_TEST, 
                agent_frecuency = FREQUENCY,
                environment_reset_client = environment_reset_client,
                environment_step_client = environment_step_client)
    
    #run_func(0)
    async_.run_async(num_processes, run_func)


if __name__ == '__main__':
    rospy.init_node('environment_step_client')
    main()




