from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division
from __future__ import absolute_import
from builtins import *  # NOQA
from future import standard_library
standard_library.install_aliases()

import argparse
import os
import sys
import distutils

import gym
gym.undo_logger_setup()
from gym import spaces
import gym.wrappers
import numpy as np

import chainer
from chainer import optimizers
from chainer import cuda
import chainerrl
from chainerrl.agents.dqn import DQN
from chainerrl import experiments
from chainerrl import explorers
from chainerrl import misc
from chainerrl import q_functions
from chainerrl import replay_buffer
from chainerrl.functions.bound_by_tanh import bound_by_tanh

from configobj import ConfigObj


STATE_BASED_ON_IMAGE = False

class NAF:
    """docstring for NAF"""
    def __init__(self, env_info_list, configs_path):
        self.name = 'NAF' # name for uploading results
        self.configs_path = configs_path
        
        config = ConfigObj(configs_path)
        
        self.gpu_flag = int(config['NAF_CHAINER']['USE_GPU_FLAG'])
        self.learning_rate = float(config['NAF_CHAINER']['LEARNING_RATE'])
        self.num_steps = int(config['NAF_CHAINER']['NUM_STEPS'])
        self.episodic_replay_flag = bool(distutils.util.strtobool(config['NAF_CHAINER']['EPISODIC_REPLAY_FLAG']))
        self.prioritized_replay_flag = bool(distutils.util.strtobool(config['NAF_CHAINER']['PRIORITIZED_REPLAY_FLAG']))
        self.replay_start_size = int(config['NAF_CHAINER']['REPLAY_START_SIZE'])
        self.target_update_interval = int(config['NAF_CHAINER']['TARGET_UPDATE_INTERVAL'])
        self.target_update_method = config['NAF_CHAINER']['TARGET_UPDATE_METHOD']
        self.soft_update_tau = float(config['NAF_CHAINER']['TAU'])
        self.update_interval = int(config['NAF_CHAINER']['UPDATE_INTERVAL'])
        self.n_hidden_channels = int(config['NAF_CHAINER']['N_HIDDEN_CHANNELS'])
        self.n_hidden_layers = int(config['NAF_CHAINER']['N_HIDDEN_LAYERS'])
        self.gamma = float(config['NAF_CHAINER']['GAMMA'])
        self.minibatch_size = int(config['NAF_CHAINER']['MINIBATCH_SIZE'])

        print ('LEARNING_RATE: ', self.learning_rate)
        print ('NUM_STEPS: ', self.num_steps)
        print ('EPISODIC REPLAY FLAG: ', self.episodic_replay_flag)
        print ('PRIORITIZED REPLAY FLAG: ', self.prioritized_replay_flag)     
        print ('REPLAY START SIZE: ', self.replay_start_size)
        print ('TARGET UPDATE INTERVAL: ', self.target_update_interval)
        print ('TARGET UPDATE METHOD: ', self.target_update_method)
        print ('TAU: ', self.soft_update_tau)
        print ('UPDATE INTERVAL: ', self.update_interval)
        print ('N HIDDEN CHANNELS: ', self.n_hidden_channels)
        print ('N HIDDEN LAYERS: ', self.n_hidden_layers)
        print ('GAMMA: ', self.gamma)
        print ('MINIBATCH SIZE: ', self.minibatch_size)
        if self.episodic_replay_flag:
            print('EPISODIC REPLAY MODE!')
        elif self.prioritized_replay_flag:
            print('PRIORITIZED REPLAY MODE!')
        else:
            print('STANDARD REPLAY BUFFER...')
        
        if not STATE_BASED_ON_IMAGE:
            self.state_dim = env_info_list[0]
        else:
            self.state_dim = env_info_list[1]
        self.action_dim = env_info_list[4]
        self.action_min = env_info_list[5]
        self.action_max = env_info_list[6]
        self.action_space = spaces.Box(np.array(self.action_min), np.array(self.action_max), dtype='float32')
        
        # Create the Q Function: Use NAF to apply DQN to continuous action spaces
        q_func = q_functions.FCQuadraticStateQFunction(
            self.state_dim, self.action_dim,
            n_hidden_channels=self.n_hidden_channels,
            n_hidden_layers=self.n_hidden_layers,
            action_space=self.action_space)
            
        # Create the EXPLORER: In this case, Ornstein-Uhlenbeck process for exploration
        ou_sigma = (self.action_space.high - self.action_space.low) * 0.2
        explorer = explorers.AdditiveOU(sigma=ou_sigma)
        
        # Create the OPTIMIZER
        opt = optimizers.Adam(alpha=self.learning_rate)
        opt.setup(q_func)
        
        # Create the REPLAY BUFFER
        rbuf_capacity = 5 * 10 ** 5
        if self.episodic_replay_flag:
            if self.minibatch_size is None:
                self.minibatch_size = 4
            if self.prioritized_replay_flag:
                betasteps = (self.num_steps - self.replay_start_size) \
                    // self.update_interval
                rbuf = replay_buffer.PrioritizedEpisodicReplayBuffer(
                    rbuf_capacity, betasteps=betasteps)
            else:
                rbuf = replay_buffer.EpisodicReplayBuffer(rbuf_capacity)
        else:
            if self.minibatch_size is None:
                self.minibatch_size = 32
            if self.prioritized_replay_flag:
                betasteps = (self.num_steps - self.replay_start_size) \
                    // self.update_interval
                rbuf = replay_buffer.PrioritizedReplayBuffer(
                    rbuf_capacity, betasteps=betasteps)
            else:
                rbuf = replay_buffer.ReplayBuffer(rbuf_capacity)
        
        self.phi = lambda x: x.astype(np.float32, copy=False)
        
        self.agent = DQN(q_func, opt, rbuf, gpu=self.gpu_flag, gamma=self.gamma,
        explorer=explorer, replay_start_size=self.replay_start_size,
        target_update_interval=self.target_update_interval,
        update_interval=self.update_interval,
        phi=self.phi, minibatch_size=self.minibatch_size,
        target_update_method=self.target_update_method,
        soft_update_tau=self.soft_update_tau,
        episodic_update=self.episodic_replay_flag, episodic_update_len=16)

    def act_and_train(self, state, reward):
        action = self.agent.act_and_train(state, reward)
        x = chainer.Variable(action)
        action_bounded = bound_by_tanh(x, self.action_space.low[0], self.action_space.high[0])
        return cuda.to_cpu(action_bounded.data)

    def act(self, state):
        action = self.agent.act(state)
        x = chainer.Variable(action)
        action_bounded = bound_by_tanh(x, self.action_space.low[0], self.action_space.high[0])
        return cuda.to_cpu(action_bounded.data)
        
    def save_agent(self, time_step, mydir):
        print ('saving agent...', time_step)
        self.agent.save(mydir + '/' + 'NAF_agent_episode' + str(time_step))
        
    def load_agent(self, mydir):
        print ('loading agent...')
        self.agent.load(mydir)
