from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division
from __future__ import absolute_import
from builtins import *  # NOQA
from future import standard_library
standard_library.install_aliases()

import argparse
import os
import sys
import distutils

import gym
#gym.undo_logger_setup()
from gym import spaces
import gym.wrappers
import numpy as np

import chainer
from chainer import optimizers
from chainer import cuda
import chainerrl
from chainerrl.agents.ddpg import DDPG
from chainerrl.agents.ddpg import DDPGModel
from chainerrl import experiments
from chainerrl import explorers
from chainerrl import misc
from chainerrl import policy
from chainerrl import q_functions
from chainerrl import replay_buffer
from chainerrl.functions.bound_by_tanh import bound_by_tanh

from configobj import ConfigObj


STATE_BASED_ON_IMAGE = False

class DDPG_CHAINER:
    """docstring for DDPG"""
    def __init__(self, env_info_list, configs_path):
        self.name = 'DDPG' # name for uploading results
        self.configs_path = configs_path
        
        config = ConfigObj(configs_path)
        
        self.gpu_flag = int(config['DDPG_CHAINER']['USE_GPU_FLAG'])
        self.actor_lr = float(config['DDPG_CHAINER']['ACTOR_LR'])
        self.critic_lr = float(config['DDPG_CHAINER']['CRITIC_LR'])
        self.n_update_times = int(config['DDPG_CHAINER']['N_UPDATE_TIMES'])
        self.replay_start_size = int(config['DDPG_CHAINER']['REPLAY_START_SIZE'])
        self.target_update_interval = int(config['DDPG_CHAINER']['TARGET_UPDATE_INTERVAL'])
        self.target_update_method = config['DDPG_CHAINER']['TARGET_UPDATE_METHOD']
        self.soft_update_tau = float(config['DDPG_CHAINER']['TAU'])
        self.update_interval = int(config['DDPG_CHAINER']['UPDATE_INTERVAL'])
        self.n_hidden_channels = int(config['DDPG_CHAINER']['N_HIDDEN_CHANNELS'])
        self.n_hidden_layers = int(config['DDPG_CHAINER']['N_HIDDEN_LAYERS'])
        self.gamma = float(config['DDPG_CHAINER']['GAMMA'])
        self.minibatch_size = int(config['DDPG_CHAINER']['MINIBATCH_SIZE'])
        self.use_bn_flag = bool(distutils.util.strtobool(config['DDPG_CHAINER']['USE_BN_FLAG']))

        print ('USE GPU FLAG: ', self.gpu_flag)
        print ('ACTOR LR: ', self.actor_lr)
        print ('CRITIC LR: ', self.critic_lr)
        print ('N UPDATE TIMES: ', self.n_update_times)     
        print ('REPLAY START SIZE: ', self.replay_start_size)
        print ('TARGET UPDATE INTERVAL: ', self.target_update_interval)
        print ('TARGET UPDATE METHOD: ', self.target_update_method)
        print ('TAU: ', self.soft_update_tau)
        print ('UPDATE INTERVAL: ', self.update_interval)
        print ('N HIDDEN CHANNELS: ', self.n_hidden_channels)
        print ('N HIDDEN LAYERS: ', self.n_hidden_layers)
        print ('GAMMA: ', self.gamma)
        print ('MINIBATCH SIZE: ', self.minibatch_size)

        
        if not STATE_BASED_ON_IMAGE:
            self.state_dim = env_info_list[0]
        else:
            self.state_dim = env_info_list[1]
        self.action_dim = env_info_list[4]
        self.action_min = env_info_list[5]
        self.action_max = env_info_list[6]
        self.action_space = spaces.Box(np.array(self.action_min), np.array(self.action_max))
        
        # Create the Q Function
        if self.use_bn_flag:
            q_func = q_functions.FCBNLateActionSAQFunction(
                self.state_dim, self.action_dim,
                n_hidden_channels=self.n_hidden_channels,
                n_hidden_layers=self.n_hidden_layers,
                normalize_input=True)
            pi = policy.FCBNDeterministicPolicy(
                self.state_dim, action_size=self.action_dim,
                n_hidden_channels=self.n_hidden_channels,
                n_hidden_layers=self.n_hidden_layers,
                min_action=self.action_space.low, max_action=self.action_space.high,
                bound_action=True,
                normalize_input=True)
        else:
            q_func = q_functions.FCSAQFunction(
                self.state_dim, self.action_dim,
                n_hidden_channels=self.n_hidden_channels,
                n_hidden_layers=self.n_hidden_layers)
            pi = policy.FCDeterministicPolicy(
                self.state_dim, action_size=self.action_dim,
                n_hidden_channels=self.n_hidden_channels,
                n_hidden_layers=self.n_hidden_layers,
                min_action=self.action_space.low, max_action=self.action_space.high,
                bound_action=True)
        model = DDPGModel(q_func=q_func, policy=pi)
        opt_a = optimizers.Adam(alpha=self.actor_lr)
        opt_c = optimizers.Adam(alpha=self.critic_lr)
        opt_a.setup(model['policy'])
        opt_c.setup(model['q_function'])
        opt_a.add_hook(chainer.optimizer.GradientClipping(1.0), 'hook_a')
        opt_c.add_hook(chainer.optimizer.GradientClipping(1.0), 'hook_c')


        # Create the REPLAY BUFFER
        rbuf = replay_buffer.ReplayBuffer(10 ** 6)
            
        # Create the EXPLORER: In this case, Ornstein-Uhlenbeck process for exploration
        ou_sigma = (self.action_space.high - self.action_space.low) * 0.2
        explorer = explorers.AdditiveOU(sigma=ou_sigma)

        self.phi = lambda x: x.astype(np.float32, copy=False)      

        # Create the AGENT
        self.agent = DDPG(model, opt_a, opt_c, rbuf, gamma=self.gamma,
                     explorer=explorer, replay_start_size=self.replay_start_size,
                     target_update_method=self.target_update_method,
                     target_update_interval=self.target_update_interval,
                     update_interval=self.update_interval,
                     soft_update_tau=self.soft_update_tau,
                     n_times_update=self.n_update_times,
                     phi=self.phi, gpu=self.gpu_flag, minibatch_size=self.minibatch_size)
    
    def act_and_train(self, state, reward):
        action = self.agent.act_and_train(state, reward)
        x = chainer.Variable(action)
        action_bounded = bound_by_tanh(x, self.action_space.low[0], self.action_space.high[0])
        return cuda.to_cpu(action_bounded.data)

    def act(self, state):
        action = self.agent.act(state)
        x = chainer.Variable(action)
        action_bounded = bound_by_tanh(x, self.action_space.low[0], self.action_space.high[0])
        return cuda.to_cpu(action_bounded.data)
 
    def save_agent(self, time_step, mydir):
        print ('saving agent...', time_step)
        self.agent.save(mydir + '/' + 'DDPG_CHAINER_agent_episode' + str(time_step))
        
    def load_agent(self, mydir):
        print ('loading agent...')
        self.agent.load(mydir)
