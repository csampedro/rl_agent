from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import
from builtins import *  # NOQA
from future import standard_library
standard_library.install_aliases()  # NOQA
import argparse


import gym
gym.undo_logger_setup()  # NOQA
from gym import spaces
import gym.wrappers
import numpy as np

import chainer
from chainer import functions as F
from chainer import cuda

import chainerrl
from chainerrl.agents import a3c
from chainerrl.agents import PPO
from chainerrl import experiments
from chainerrl import links
from chainerrl import misc
from chainerrl.optimizers.nonbias_weight_decay import NonbiasWeightDecay
from chainerrl import policies
from chainerrl import wrappers
from chainerrl.functions.bound_by_tanh import bound_by_tanh

from configobj import ConfigObj


STATE_BASED_ON_IMAGE = False


class A3CFFSoftmax(chainer.ChainList, a3c.A3CModel):
    """An example of A3C feedforward softmax policy."""

    def __init__(self, ndim_obs, n_actions, hidden_sizes=(200, 200)):
        self.pi = policies.SoftmaxPolicy(
            model=links.MLP(ndim_obs, n_actions, hidden_sizes))
        self.v = links.MLP(ndim_obs, 1, hidden_sizes=hidden_sizes)
        super().__init__(self.pi, self.v)

    def pi_and_v(self, state):
        return self.pi(state), self.v(state)


class A3CFFMellowmax(chainer.ChainList, a3c.A3CModel):
    """An example of A3C feedforward mellowmax policy."""

    def __init__(self, ndim_obs, n_actions, hidden_sizes=(200, 200)):
        self.pi = policies.MellowmaxPolicy(
            model=links.MLP(ndim_obs, n_actions, hidden_sizes))
        self.v = links.MLP(ndim_obs, 1, hidden_sizes=hidden_sizes)
        super().__init__(self.pi, self.v)

    def pi_and_v(self, state):
        return self.pi(state), self.v(state)


class A3CFFGaussian(chainer.Chain, a3c.A3CModel):
    """An example of A3C feedforward Gaussian policy."""

    def __init__(self, obs_size, action_space,
                 n_hidden_layers=2, n_hidden_channels=64,
                 bound_mean=None, normalize_obs=None):
        assert bound_mean in [False, True]
        assert normalize_obs in [False, True]
        super().__init__()
        hidden_sizes = (n_hidden_channels,) * n_hidden_layers
        self.normalize_obs = normalize_obs
        with self.init_scope():
            self.pi = policies.FCGaussianPolicyWithStateIndependentCovariance(
                obs_size, action_space.low.size,
                n_hidden_layers, n_hidden_channels,
                var_type='diagonal', nonlinearity=F.tanh,
                bound_mean=bound_mean,
                min_action=action_space.low, max_action=action_space.high,
                mean_wscale=1e-2)
            self.v = links.MLP(obs_size, 1, hidden_sizes=hidden_sizes)
            if self.normalize_obs:
                self.obs_filter = links.EmpiricalNormalization(
                    shape=obs_size
                )

    def pi_and_v(self, state):
        if self.normalize_obs:
            state = F.clip(self.obs_filter(state, update=False),
                           -5.0, 5.0)

        return self.pi(state), self.v(state)



class PPO_CHAINER:
    """docstring for PPO"""
    def __init__(self,  env_info_list, configs_path):
        self.name = 'PPO' # name for uploading results
        self.configs_path = configs_path
        
        config = ConfigObj(configs_path)
        
        self.gpu_flag = int(config['PPO_CHAINER']['USE_GPU_FLAG'])
        self.architecture_type = config['PPO_CHAINER']['ARCHITECTURE']
        self.num_steps = int(config['PPO_CHAINER']['NUM_STEPS'])
        self.learning_rate = float(config['PPO_CHAINER']['LEARNING_RATE'])
        self.weight_decay = float(config['PPO_CHAINER']['WEIGHT_DECAY'])
        self.update_interval = int(config['PPO_CHAINER']['UPDATE_INTERVAL'])
        self.batch_size = int(config['PPO_CHAINER']['MINIBATCH_SIZE'])
        self.epochs = int(config['PPO_CHAINER']['EPOCHS'])
        self.entropy_coef = float(config['PPO_CHAINER']['ENTROPY_COEF'])
        self.n_hidden_channels = int(config['PPO_CHAINER']['N_HIDDEN_CHANNELS'])
        self.n_hidden_layers = int(config['PPO_CHAINER']['N_HIDDEN_LAYERS'])
        self.seed = 0
        
        print ('USE GPU FLAG: ', self.gpu_flag)
        print ('ARCHITECTURE: ', self.architecture_type)
        print ('NUM STEPS: ', self.num_steps)
        print ('LEARNING RATE: ', self.learning_rate)
        print ('WEIGHT DECAY: ', self.weight_decay)
        print ('UPDATE_INTERVAL: ', self.update_interval)
        print ('MINIBATCH_SIZE: ', self.batch_size)
        print ('EPOCHS: ', self.epochs)
        print ('ENTROPY_COEF: ', self.entropy_coef)
        print ('N HIDDEN CHANNELS: ', self.n_hidden_channels)
        print ('N HIDDEN LAYERS: ', self.n_hidden_layers)
        
        if not STATE_BASED_ON_IMAGE:
            self.state_dim = env_info_list[0]
        else:
            self.state_dim = env_info_list[1]
        self.action_dim = env_info_list[4]
        self.action_min = env_info_list[5]
        self.action_max = env_info_list[6]
        self.action_space = spaces.Box(np.array(self.action_min), np.array(self.action_max), dtype='float32')
        
        # Set a random seed used in ChainerRL
        misc.set_random_seed(self.seed, gpus=(self.gpu_flag,))

        
        # Switch policy types accordingly to action space types
        if self.architecture_type == 'FFSoftmax':
            model = A3CFFSoftmax(self.state_dim, self.action_dim)
        elif self.architecture_type == 'FFMellowmax':
            model = A3CFFMellowmax(self.state_dim, self.action_dim)
        elif self.architecture_type == 'FFGaussian':
            model = A3CFFGaussian(self.state_dim, self.action_space,
                                    n_hidden_layers=self.n_hidden_layers, 
                                    n_hidden_channels=self.n_hidden_channels,
                                    bound_mean=True,
                                    normalize_obs=True)
            #~ model = A3CFFGaussian(self.state_dim, self.action_space,
                                  #~ bound_mean=args.bound_mean,
                                  #~ normalize_obs=args.normalize_obs)

        opt = chainer.optimizers.Adam(alpha=self.learning_rate, eps=1e-5)
        opt.setup(model)
        if self.weight_decay > 0:
            opt.add_hook(NonbiasWeightDecay(self.weight_decay))
        self.agent = PPO(model, opt,
                    gpu=self.gpu_flag,
                    update_interval=self.update_interval,
                    minibatch_size=self.batch_size, epochs=self.epochs,
                    clip_eps_vf=None, entropy_coef=self.entropy_coef,
                    )
            

    def act_and_train(self, state, reward):
        state = np.float32(state)
        action = self.agent.act_and_train(state, reward)
        x = chainer.Variable(action)
        action_bounded = bound_by_tanh(x, self.action_space.low[0], self.action_space.high[0])
        return cuda.to_cpu(action_bounded.data)
        
    def stop_episode_and_train(self, state, reward, done=False):
        self.agent.stop_episode_and_train(state, reward, done)

    def act(self, state):
        action = self.agent.act(state)
        x = chainer.Variable(action)
        action_bounded = bound_by_tanh(x, self.action_space.low[0], self.action_space.high[0])
        return cuda.to_cpu(action_bounded.data)
 
    def save_agent(self, time_step, mydir):
        print ('saving agent...', time_step)
        self.agent.save(mydir + '/' + 'A3C_CHAINER_agent_episode' + str(time_step))
        
    def load_agent(self, mydir):
        print ('loading agent...')
        self.agent.load(mydir)        
