#!/usr/bin/env python

import sys

import rospy
from sensor_msgs.msg import Image
from rl_agent.srv import *
import std_srvs
from std_srvs.srv import Empty
from std_msgs.msg import Float32MultiArray
from std_msgs.msg import MultiArrayDimension
from ddpg_chainer import *
from droneMsgsROS.msg import droneSpeeds
import csv

DEBUG_SERVICES_MODE = False

class ChainerAgentRosModule:
    def __init__(self):
        self.ReadParameters()
        self.uav_speed_ref_pub_ = rospy.Publisher('/drone11/droneSpeedsRefs', droneSpeeds, queue_size=10)
        self.uav_state_actions_pub_ = rospy.Publisher('/drone11/droneRlStateActions', Float32MultiArray, queue_size=10)
        self.rl_env_state_subs_ = rospy.Subscriber('rl_environment/state', Float32MultiArray, self.EnvironmentStateCallback)
        
        self.env_info_list = self.EnvironmentDimensionalityClient()
        print 'self.env_info_list: ', self.env_info_list
        self.agent = DDPG_CHAINER(self.env_info_list, self.configs_path)
        self.agent.load_agent(self.saved_agent_path)

        
    def ReadParameters(self):
        self.configs_path = rospy.get_param('/chainer_agent_ros_module/configs_path')
        self.exp_logger_path = rospy.get_param('/chainer_agent_ros_module/exp_logger_path')
        self.saved_agent_path = rospy.get_param('/chainer_agent_ros_module/saved_agent_path')
        self.frequency = rospy.get_param('/chainer_agent_ros_module/frecuency')
        
        print 'self.configs_path: ', self.configs_path
        print 'self.exp_logger_path: ', self.exp_logger_path
        print 'saved_agent_path: ', self.saved_agent_path
        print 'FREQUENCY: ', self.frequency


    def EnvironmentDimensionalityClient(self):
        rospy.wait_for_service('rl_env_dimensionality_srv')
        try:
            print 'Executing service proxy...'
            environment_dimensionality = rospy.ServiceProxy('rl_env_dimensionality_srv', EnvDimensionalitySrv)
            resp = environment_dimensionality()
            return [resp.state_dim_lowdim, resp.state_dim_img, resp.state_min, resp.state_max, resp.action_dim, resp.action_min, resp.action_max, resp.num_iterations]
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e
            
            
    
    def EnvironmentResetClient(self):
        rospy.wait_for_service('rl_env_reset_srv')
        try:
            environment_reset = rospy.ServiceProxy('rl_env_reset_srv', ResetEnvSrv)
            resp = environment_reset()
            
            print '------- Response (env reset) -------'
            print 'resp.state: ', resp.state
            
            image_response = resp.img_state
            if(len(image_response) !=0):
                cv_bridge_obj = CvBridge()
                image_ini = cv_bridge_obj.imgmsg_to_cv2(image_response[0], "mono8")
                images_array = np.empty((image_ini.shape[0], image_ini.shape[1], len(image_response)))
                print 'len(image_response): ', len(image_response)
                for i in range(len(image_response)):
                    image_np = cv_bridge_obj.imgmsg_to_cv2(image_response[i], "mono8")
                    image_np_norm = np.multiply(image_np, 1.0 / 255.0)
                    images_array[:,:,i] = np.reshape(image_np_norm, (image_np_norm.shape[0], image_np_norm.shape[1]))
                print 'images_array (shape): ', images_array.shape
                return resp.state, images_array
            else:
                return resp.state, image_response
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e    
        
            
    def EnvironmentStepClient(self, action):
        #print 'waiting for server (step_client)...'
        rospy.wait_for_service('rl_env_step_srv')
        try:
            environment_step = rospy.ServiceProxy('rl_env_step_srv', AgentSrv)
            resp = environment_step(action)
            
            if DEBUG_SERVICES_MODE:
                print '------- Response (env step) -------'
                print 'resp.reward: ', resp.reward
                print 'resp.state: ', resp.obs_real
                print 'resp.terminal_state: ', resp.terminal_state
                print 'resp.img (type): ', type(image_np)
            
            image_response = resp.img
            if(len(image_response) !=0):
                cv_bridge_obj = CvBridge()
                image_ini = cv_bridge_obj.imgmsg_to_cv2(image_response[0], "mono8")
                images_array = np.empty((image_ini.shape[0], image_ini.shape[1], len(image_response)))
                for i in range(len(image_response)):
                    image_np = cv_bridge_obj.imgmsg_to_cv2(image_response[i], "mono8")
                    image_np_norm = np.multiply(image_np, 1.0 / 255.0)
                    images_array[:,:,i] = np.reshape(image_np_norm, (image_np_norm.shape[0], image_np_norm.shape[1]))
                return resp.obs_real, resp.reward, resp.terminal_state, images_array
            else:
                return resp.obs_real, resp.reward, resp.terminal_state, image_response
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e
        
        
    def EnvironmentStateCallback(self, data):
        #rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data)
        
        state_resp = data.data
        state = np.array(state_resp)
        action = self.agent.agent.act(state)
        
        if not np.isfinite(action).all():
            action = np.zeros(len(action))
            
        action_msg = droneSpeeds()
        action_msg.dx = action[0]
        action_msg.dy = action[1]
        action_msg.dz = 0.0
        self.uav_speed_ref_pub_.publish(action_msg)

        state_array = np.asarray(state)
        state_actions_array = np.concatenate((action, state_array))
        rl_state_ations_msg = Float32MultiArray()
        rl_state_ations_msg.layout.dim.append(MultiArrayDimension())
        rl_state_ations_msg.layout.dim[0].size = len(state_actions_array)
        rl_state_ations_msg.layout.dim[0].stride = 1
        rl_state_ations_msg.layout.dim[0].label = "x"
        rl_state_ations_msg.data = state_actions_array
        self.uav_state_actions_pub_.publish(rl_state_ations_msg)
        #~ 
        #~ 
        next_state_resp, reward, done, img = self.EnvironmentStepClient(action)
        #~ #print 'reward: ', reward;

