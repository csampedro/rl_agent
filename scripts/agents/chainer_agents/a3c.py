from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import
from builtins import *  # NOQA
from future import standard_library
standard_library.install_aliases()  # NOQA
import argparse
import os

# This prevents numpy from using multiple threads
os.environ['OMP_NUM_THREADS'] = '1'  # NOQA

import gym
gym.undo_logger_setup()  # NOQA
from gym import spaces
import gym.wrappers
import numpy as np

import chainer
from chainer import functions as F
from chainer import links as L
from chainer import cuda
import chainerrl
from chainerrl.agents import a3c
from chainerrl import experiments
from chainerrl import links
from chainerrl import misc
from chainerrl.optimizers.nonbias_weight_decay import NonbiasWeightDecay
from chainerrl.optimizers import rmsprop_async
from chainerrl import policies
from chainerrl.recurrent import RecurrentChainMixin
from chainerrl import v_function
from chainerrl import wrappers
from chainerrl.functions.bound_by_tanh import bound_by_tanh

from configobj import ConfigObj


STATE_BASED_ON_IMAGE = False


class A3CFFSoftmax(chainer.ChainList, a3c.A3CModel):
    """An example of A3C feedforward softmax policy."""

    def __init__(self, ndim_obs, n_actions, hidden_sizes=(200, 200)):
        self.pi = policies.SoftmaxPolicy(
            model=links.MLP(ndim_obs, n_actions, hidden_sizes))
        self.v = links.MLP(ndim_obs, 1, hidden_sizes=hidden_sizes)
        super().__init__(self.pi, self.v)

    def pi_and_v(self, state):
        return self.pi(state), self.v(state)


class A3CFFMellowmax(chainer.ChainList, a3c.A3CModel):
    """An example of A3C feedforward mellowmax policy."""

    def __init__(self, ndim_obs, n_actions, hidden_sizes=(200, 200)):
        self.pi = policies.MellowmaxPolicy(
            model=links.MLP(ndim_obs, n_actions, hidden_sizes))
        self.v = links.MLP(ndim_obs, 1, hidden_sizes=hidden_sizes)
        super().__init__(self.pi, self.v)

    def pi_and_v(self, state):
        return self.pi(state), self.v(state)


class A3CLSTMGaussian(chainer.ChainList, a3c.A3CModel, RecurrentChainMixin):
    """An example of A3C recurrent Gaussian policy."""

    def __init__(self, obs_size, action_size, hidden_size=200, lstm_size=128):
        self.pi_head = L.Linear(obs_size, hidden_size)
        self.v_head = L.Linear(obs_size, hidden_size)
        self.pi_lstm = L.LSTM(hidden_size, lstm_size)
        self.v_lstm = L.LSTM(hidden_size, lstm_size)
        self.pi = policies.LinearGaussianPolicyWithDiagonalCovariance(
            lstm_size, action_size)
        self.v = v_function.FCVFunction(lstm_size)
        super().__init__(self.pi_head, self.v_head,
                         self.pi_lstm, self.v_lstm, self.pi, self.v)

    def pi_and_v(self, state):

        def forward(head, lstm, tail):
            h = F.relu(head(state))
            h = lstm(h)
            return tail(h)

        pout = forward(self.pi_head, self.pi_lstm, self.pi)
        vout = forward(self.v_head, self.v_lstm, self.v)

        return pout, vout


class A3C_CHAINER:
    """docstring for A3C"""
    def __init__(self,  env_info_list, configs_path):
        self.name = 'A3C' # name for uploading results
        self.configs_path = configs_path
        
        config = ConfigObj(configs_path)
        
        self.gpu_flag = int(config['A3C_CHAINER']['USE_GPU_FLAG'])
        self.architecture_type = config['A3C_CHAINER']['ARCHITECTURE']
        self.t_max = config['A3C_CHAINER']['T_MAX']
        self.beta = config['A3C_CHAINER']['BETA']
        self.num_steps = config['A3C_CHAINER']['NUM_STEPS']
        self.rmsprop_epsilon = config['A3C_CHAINER']['RMSPROP_EPSILON']
        self.learning_rate = config['A3C_CHAINER']['LEARNING_RATE']
        self.weight_decay = config['A3C_CHAINER']['WEIGHT_DECAY']
        self.seed = 0
        
        print ('USE GPU FLAG: ', self.gpu_flag)
        print ('ARCHITECTURE: ', self.architecture_type)
        print ('t_MAX: ', self.t_max)     
        print ('BETA: ', self.beta)
        print ('NUM STEPS: ', self.num_steps)
        print ('RMSPROP EPSILON: ', self.rmsprop_epsilon)
        print ('LEARNING RATE: ', self.learning_rate)
        print ('WEIGHT DECAY: ', self.weight_decay)
        
        if not STATE_BASED_ON_IMAGE:
            self.state_dim = env_info_list[0]
        else:
            self.state_dim = env_info_list[1]
        self.action_dim = env_info_list[4]
        self.action_min = env_info_list[5]
        self.action_max = env_info_list[6]
        self.action_space = spaces.Box(np.array(self.action_min), np.array(self.action_max), dtype='float32')
        
        # Set a random seed used in ChainerRL.
        # If you use more than one processes, the results will be no longer
        # deterministic even with the same random seed.
        misc.set_random_seed(self.seed)

        
        # Switch policy types accordingly to action space types
        if self.architecture_type == 'LSTMGaussian':
            model = A3CLSTMGaussian(self.state_dim, self.action_dim)
        elif self.architecture_type == 'FFSoftmax':
            model = A3CFFSoftmax(self.state_dim, self.action_dim)
        elif self.architecture_type == 'FFMellowmax':
            model = A3CFFMellowmax(self.state_dim, self.action_dim)

        opt = rmsprop_async.RMSpropAsync(lr=self.learning_rate, eps=self.rmsprop_epsilon, alpha=0.99)
        opt.setup(model)
        opt.add_hook(chainer.optimizer.GradientClipping(40))
        if self.weight_decay > 0:
            opt.add_hook(NonbiasWeightDecay(self.weight_decay))

        #Create an instance of the A3C agent
        self.agent = a3c.A3C(model, opt, t_max=self.t_max, gamma=0.99, beta=self.beta)
        

    def act_and_train(self, state, reward):
        #~ print('old state: ' + str(state))
        state = np.float32(state)
        #~ print('state: ' + str(state))
        #~ print('state.dtype: ' + str(state.dtype))
        action = self.agent.act_and_train(state, reward)
        #~ print('action: ' + str(action))
        x = chainer.Variable(action)
        action_bounded = bound_by_tanh(x, self.action_space.low[0], self.action_space.high[0])
        return cuda.to_cpu(action_bounded.data)
        
    def stop_episode_and_train(self, state, reward, done=False):
        self.agent.stop_episode_and_train(state, reward, done)

    def act(self, state):
        action = self.agent.act(state)
        x = chainer.Variable(action)
        action_bounded = bound_by_tanh(x, self.action_space.low[0], self.action_space.high[0])
        return cuda.to_cpu(action_bounded.data)
 
    def save_agent(self, time_step, mydir):
        print ('saving agent...', time_step)
        self.agent.save(mydir + '/' + 'A3C_CHAINER_agent_episode' + str(time_step))
        
    def load_agent(self, mydir):
        print ('loading agent...')
        self.agent.load(mydir)        
