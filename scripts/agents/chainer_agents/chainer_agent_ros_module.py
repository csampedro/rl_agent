#!/usr/bin/env python

from chainer_agent_ros_module_class import *
import rospy
STATE_BASED_ON_IMAGE = False

def agent_ros_module_server():
    rospy.init_node('chainer_agent_ros_module')
    rate = rospy.Rate(20) # 20hz
    agent = ChainerAgentRosModule()
    state_resp, img_resp = agent.EnvironmentResetClient()
    if not STATE_BASED_ON_IMAGE:
        state = np.array(state_resp)
    else:
        state = img_resp
        
    while not rospy.is_shutdown():
        action = agent.agent.act(state) # direct action for test
        state_resp, reward, done, image_state = agent.EnvironmentStepClient(action)
        state = np.array(state_resp)
        rate.sleep()

if __name__ == '__main__':
	agent_ros_module_server()


