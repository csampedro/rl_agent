from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import
from builtins import *  # NOQA
from future import standard_library
standard_library.install_aliases()  # NOQA

import argparse
import logging
import os

import chainer
from chainer import functions as F
from chainer import cuda
import gym
#gym.undo_logger_setup()  # NOQA
import gym.wrappers
from gym import spaces
import numpy as np

import chainerrl
from chainerrl import misc
from chainerrl import wrappers
from chainerrl.functions.bound_by_tanh import bound_by_tanh

from configobj import ConfigObj


STATE_BASED_ON_IMAGE = False



class TRPO_CHAINER:
    """docstring for TRPO"""
    def __init__(self,  env_info_list, configs_path):
        self.name = 'TRPO' # name for uploading results
        self.configs_path = configs_path
        
        config = ConfigObj(configs_path)
        
        self.gpu_flag = int(config['TRPO_CHAINER']['USE_GPU_FLAG'])
        self.num_steps = int(config['TRPO_CHAINER']['NUM_STEPS']
        self.learning_rate = float(config['TRPO_CHAINER']['LEARNING_RATE'])
        self.update_interval = int(config['TRPO_CHAINER']['UPDATE_INTERVAL']
        self.n_hidden_channels = int(config['TRPO_CHAINER']['N_HIDDEN_CHANNELS'])
        self.n_hidden_layers = int(config['TRPO_CHAINER']['N_HIDDEN_LAYERS'])
        self.seed = 0
        
        print ('USE GPU FLAG: ', self.gpu_flag)
        print ('NUM STEPS: ', self.num_steps)
        print ('LEARNING RATE: ', self.learning_rate)
        print ('UPDATE_INTERVAL: ', self.update_interval)
        print ('N HIDDEN CHANNELS: ', self.n_hidden_channels)
        print ('N HIDDEN LAYERS: ', self.n_hidden_layers)
        
        if not STATE_BASED_ON_IMAGE:
            self.state_dim = env_info_list[0]
        else:
            self.state_dim = env_info_list[1]
        self.action_dim = env_info_list[4]
        self.action_min = env_info_list[5]
        self.action_max = env_info_list[6]
        self.action_space = spaces.Box(np.array(self.action_min), np.array(self.action_max), dtype='float32')
        
        # Set a random seed used in ChainerRL
        misc.set_random_seed(self.seed, gpus=(self.gpu_flag,))

        # Normalize observations based on their empirical mean and variance
        obs_normalizer = chainerrl.links.EmpiricalNormalization(self.state_dim)

        # Use a Gaussian policy for continuous action spaces
        policy = \
            chainerrl.policies.FCGaussianPolicyWithStateIndependentCovariance(
                self.state_dim,
                self.action_dim,
                n_hidden_channels=self.n_hidden_channels,
                n_hidden_layers=self.n_hidden_layers,
                mean_wscale=0.01,
                nonlinearity=F.tanh,
                var_type='diagonal',
                var_func=lambda x: F.exp(2 * x),  # Parameterize log std
                var_param_init=0,  # log std = 0 => std = 1
            )
        
        
        # Use a value function to reduce variance
        vf = chainerrl.v_functions.FCVFunction(
            self.state_dim,
            n_hidden_channels=self.n_hidden_channels,
            n_hidden_layers=self.n_hidden_layers,
            last_wscale=0.01,
            nonlinearity=F.tanh,
        )

        if self.gpu_flag >= 0:
            chainer.cuda.get_device_from_id(self.gpu_flag).use()
            policy.to_gpu(self.gpu_flag)
            vf.to_gpu(self.gpu_flag)
            obs_normalizer.to_gpu(self.gpu_flag)

        # TRPO's policy is optimized via CG and line search, so it doesn't require
        # a chainer.Optimizer. Only the value function needs it.
        vf_opt = chainer.optimizers.Adam(alpha=self.learning_rate)
        vf_opt.setup(vf)
        
        # Hyperparameters in http://arxiv.org/abs/1709.06560
        self.agent = chainerrl.agents.TRPO(
            policy=policy,
            vf=vf,
            vf_optimizer=vf_opt,
            obs_normalizer=obs_normalizer,
            update_interval=self.update_interval,
            conjugate_gradient_max_iter=20,
            conjugate_gradient_damping=1e-1,
            gamma=0.995,
            lambd=0.97,
            vf_epochs=5,
            entropy_coef=0,
        )
        
        

    def act_and_train(self, state, reward):
        state = np.float32(state)
        action = self.agent.act_and_train(state, reward)
        x = chainer.Variable(action)
        action_bounded = bound_by_tanh(x, self.action_space.low[0], self.action_space.high[0])
        return cuda.to_cpu(action_bounded.data)
        
    def stop_episode_and_train(self, state, reward, done=False):
        self.agent.stop_episode_and_train(state, reward, done)

    def act(self, state):
        action = self.agent.act(state)
        x = chainer.Variable(action)
        action_bounded = bound_by_tanh(x, self.action_space.low[0], self.action_space.high[0])
        return cuda.to_cpu(action_bounded.data)
 
    def save_agent(self, time_step, mydir):
        print ('saving agent...', time_step)
        self.agent.save(mydir + '/' + 'A3C_CHAINER_agent_episode' + str(time_step))
        
    def load_agent(self, mydir):
        print ('loading agent...')
        self.agent.load(mydir)        
