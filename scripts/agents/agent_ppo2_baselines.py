#!/usr/bin/env python
import sys

import numpy as np
from gym import utils
import sys
import gym
from gym import spaces
sys.path.append('/opt/ros/melodic/lib/python2.7/dist-packages')
import rospy
from rl_agent.srv import *
import time
import os

arguments = []
rl_boost_python_path = rospy.get_param('/agent/rl_boost_python_path')
rl_python_distpackages_path = rospy.get_param('/agent/rl_python_distpackages_path')
sys.path.insert(1, rl_boost_python_path)
sys.path.insert(2, rl_python_distpackages_path)


#Enviroment parameters
env = rospy.get_param('/agent/enviroment_gym')
arguments.append('--env='+env)

pause_flag = rospy.get_param('/agent/pause_flag')
arguments.append('--pause_flag='+pause_flag)



resume_training = rospy.get_param('/agent/resume_training')
test = rospy.get_param('/agent/test')


if resume_training == 'True':

	training_time_steps = rospy.get_param('/agent/training_time_steps')
	arguments.append('--num_timesteps='+training_time_steps)

	load_networks_path = rospy.get_param('/agent/load_networks_path')
	arguments.append('--load_path='+load_networks_path)


elif test == 'True':
	arguments.append('--num_timesteps=0')

	load_networks_path = rospy.get_param('/agent/load_networks_path')
	arguments.append('--load_path='+load_networks_path)

	arguments.append('--num_timesteps=0')
	arguments.append('--play')

else:

	training_time_steps = rospy.get_param('/agent/training_time_steps')
	arguments.append('--num_timesteps=' + training_time_steps)


# Network parameters


network = rospy.get_param('/agent/network')
arguments.append('--network='+ network)

num_hidden = rospy.get_param('/agent/num_hidden_units')
arguments.append('--num_hidden='+ num_hidden)

num_layers = rospy.get_param('/agent/num_hidden_layers')
arguments.append('--num_layers='+num_layers)

activation = rospy.get_param('/agent/activation')
arguments.append('--activation=' + activation)

layer_norm = rospy.get_param('/agent/layer_norm')
arguments.append('--layer_norm=' + layer_norm)

#Agent parameters 

alg = rospy.get_param('/agent/agent_type')
arguments.append('--alg='+alg)

nsteps = rospy.get_param('/agent/nsteps')
arguments.append('--nsteps='+nsteps)

ent_coef = rospy.get_param('/agent/ent_coef')
arguments.append('--ent_coef='+ent_coef)

lr = rospy.get_param('/agent/learning_rate')
arguments.append('--lr='+lr)

max_grad_norm = rospy.get_param('/agent/max_grad_norm')
arguments.append('--max_grad_norm='+max_grad_norm)

gamma = rospy.get_param('/agent/gamma')
arguments.append('--gamma='+gamma)

lam = rospy.get_param('/agent/lambda')
arguments.append('--lam='+lam)

nminibatches = rospy.get_param('/agent/nminibatches')
arguments.append('--nminibatches='+nminibatches)

noptepochs = rospy.get_param('/agent/noptepochs')
arguments.append('--noptepochs='+noptepochs)

cliprange = rospy.get_param('/agent/cliprange')
arguments.append('--cliprange='+cliprange)

vf_coef = rospy.get_param('/agent/vf_coef')
arguments.append('--vf_coef='+vf_coef)

updates_steps_between_logs = rospy.get_param('/agent/updates_steps_between_logs')
arguments.append('--log_interval='+updates_steps_between_logs)


sys.argv.extend(arguments)

import baselines.run

baselines.run.main(arguments)
