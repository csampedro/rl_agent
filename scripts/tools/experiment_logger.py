
import errno
import os
from datetime import datetime
import csv
import subprocess
import sys
import argparse
import shlex
import time

class ExperimentLogger:
    def __init__(self, is_test, results_path, variables_array):
        self.results_path = results_path
        if is_test:
            # Create subdirectory
            self.mydir = datetime.now().strftime('%Y-%m-%d_%H-%M-%S') + '_TEST'
        else:
            # Create subdirectory
            self.mydir = datetime.now().strftime('%Y-%m-%d_%H-%M-%S') + '_TRAIN'
        try:
            os.makedirs(self.results_path + self.mydir)
            self.complete_mydir = self.results_path + self.mydir
        except OSError as e:
            if e.errno != errno.EEXIST:
                print 'RL_AGENT_ERROR: Error creating directory'

        # Store the name of the variables
        self.data = ['time']
        for i in range(0, len(variables_array)):
            self.data.append(variables_array[i])

        # Init files
        with open(self.results_path + self.mydir + '/log_experiment_train.csv', 'w') as fp:
            a = csv.writer(fp, delimiter=',')
            #data = ['episode', 'accum_reward', 'accum_q_value']
            a.writerow(self.data)

        with open(self.results_path + self.mydir + '/log_experiment_test.csv', 'w') as fp:
            a = csv.writer(fp, delimiter=',')
            #data = ['episode', 'accum_reward']
            a.writerow(self.data)

        with open(self.results_path + self.mydir + '/log_experiment_steps_train.csv', 'w') as fp:
            a = csv.writer(fp, delimiter=',')
            # if state_dim == 4:
            #     data = ['episode', 'reward', 'accum_reward', 'q_value', 'accum_q_value', 'state_x', 'state_y', 'state_vx', 'state_vy', 'action_x', 'action_y']
            # elif state_dim == 2:
            #     data = ['episode', 'reward', 'accum_reward', 'q_value', 'accum_q_value', 'state_x', 'state_y', 'action_x', 'action_y']
            a.writerow(self.data)

        with open(self.results_path + self.mydir + '/log_experiment_steps_test.csv', 'w') as fp:
            a = csv.writer(fp, delimiter=',')
            # if state_dim == 4:
            #     data = ['episode', 'reward', 'accum_reward', 'state_x', 'state_y', 'state_vx', 'state_vy', 'action_x', 'action_y']
            # elif state_dim == 2:
            #     data = ['episode', 'reward', 'accum_reward', 'state_x', 'state_y', 'action_x', 'action_y']
            a.writerow(self.data)


    def get_path_resutls(self):
        return self.complete_mydir


    def log_steps_train(self, data_to_store):
        with open(self.results_path + self.mydir + '/log_experiment_steps_train.csv', 'a') as f:
            writer = csv.writer(f)
            # if len(state) == 4:
            #     writer.writerow([str(episode), str(reward), str(total_reward), str(q_value), str(total_q_value), \
            #                      str(state[0]), str(state[1]), str(state[2]), str(state[3]), str(action[0]), str(action[1])])
            # elif len(state) == 2:
            #     writer.writerow([str(episode), str(reward), str(total_reward), str(q_value), str(total_q_value), \
            #                      str(state[0]), str(state[1]), str(action[0]), str(action[1])])
            data_str = [str(time.time())]
            for i in range(0, len(data_to_store)):
                data_str.append(str(data_to_store[i]))
            writer.writerow(data_str)

    def log_steps_test(self, data_to_store):
        with open(self.results_path + self.mydir + '/log_experiment_steps_test.csv', 'a') as f:
            writer = csv.writer(f)
            # if len(state) == 4:
            #     writer.writerow([str(episode), str(reward), str(total_reward), str(state[0]), str(state[1]), \
            #     str(state[2]), str(state[3]), str(action[0]), str(action[1])])
            # elif len(state) == 2:
            #     writer.writerow([str(episode), str(reward), str(total_reward), str(state[0]), str(state[1]), str(action[0]), str(action[1])])
            data_str = [str(time.time())]
            for i in range(0, len(data_to_store)):
                data_str.append(str(data_to_store[i]))
            writer.writerow(data_str)

    def log_test(self, data_to_store):
        with open(self.results_path + self.mydir + '/log_experiment_test.csv', 'a') as f:
            writer = csv.writer(f)
            # writer.writerow([str(episode), str(accum_reward)])
            data_str = [str(time.time())]
            for i in range(0, len(data_to_store)):
                data_str.append(str(data_to_store[i]))
            writer.writerow(data_str)

    def log_train(self, data_to_store):
        with open(self.results_path + self.mydir + '/log_experiment_train.csv', 'a') as f:
            writer = csv.writer(f)
            # writer.writerow([str(episode), str(total_reward), str(total_q_value)])
            data_str = [str(time.time())]
            for i in range(0, len(data_to_store)):
                data_str.append(str(data_to_store[i]))
            writer.writerow(data_str)

    def record_video(self, episode):
        DEFAULT_RESOLUTION = "1920x1080"
        DEFAULT_CODEC = "h264"
        DEFAULT_OUTPUTFILE = self.results_path + self.mydir + "/" + str(episode) + ".avi"
        command = """ffmpeg -f x11grab -r 25 -s {0} -i :0.0 -vcodec {1} {2}""".format( \
            DEFAULT_RESOLUTION, DEFAULT_CODEC, DEFAULT_OUTPUTFILE)
        arguments = shlex.split(command)
        self.process = subprocess.Popen(arguments)
        print("grabando")

    def stop_video(self):
        self.process.kill()




